# Skript writer

The only **open source** content editor you will ever need. No more CMS's, no
more office applications, no more cloud services. Say goodby to Google Docs
surveilance capitalism, Wordpress' legacy code base, server side system
administration and mixing style and structure in office applications. Say hello
to a collaborative editor made for content creators, serverless, hackable. An
editor which is able to produce content in any style you desire. An editor which
is truly free to use - no tracking, no data profiling, no ads.